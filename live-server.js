const liveServer = require("live-server");

const params = {
    root: "./public", // Set root directory that's being served. Defaults to cwd.
    open: true, // When false, it won't load your browser by default.
    file: "index.html", // When set, serve this file for every 404 (useful for single-page applications)
};
liveServer.start(params);